package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test //utile per riconoscere che sia un metodo di test
	public void testFifteenThirty () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}

	
	@Test //utile per riconoscere che sia un metodo di test
	public void testFortyThirty () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer forty - Nadal thirty", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testPlayer1Wins () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		/*game.incrementPlayerScore(playerName1); non posso metterli qua perch� se player1 fa 4 punti , ha gia vinto e non � possibile che il player2 faccia 2 punti dopo aver gia perso
		 *game.incrementPlayerScore(playerName1); 
		*/
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer wins", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testFifteenForty () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testDeuce () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Deuce", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testAdvantagePlayer1 () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Advantage Federer", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testPlayer2Wins () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Nadal wins", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testAdvantagePlayer2 () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Advantage Nadal", status);
	}
	
	
	@Test //utile per riconoscere che sia un metodo di test
	public void testDeuceAfterAdvantage () throws GameHasAlreadyBeWonException, DuplicatedPlayerException {  
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Deuce", status);
	}
/** The Game class represents a tennis game. This class should be used as
	 * follows:
	 * 
	 * // Create a new game: 
	 * Game game = new Game("name1", "name2");
	 *
	 * // Get the name of the two players: 
	 * String playerName1 = game.getPlayerName1(); 
	 * String playerName2 = game.getPlayerName2();
	 *
	 * // Update the game by incrementing the score of either the first or second
	 * player: 
	 * game.incrementPlayerScore(playerName1);
	 * game.incrementPlayerScore(playerName2);
	 *
	 * // Get the status of the game: 
	 * String status = game.getGameStatus(); */
}
