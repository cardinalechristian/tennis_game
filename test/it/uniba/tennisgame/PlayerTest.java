package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test //utile per riconoscere che sia un metodo di test
	public void scoreShouldBeIncreased() {  //copro la prima classe di equivalenza (azione va a buon fine)
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1 ,player.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {  //copro la seconda classe di equivalenza (azione non viene eseguita)
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		
		//Assert
		assertEquals(0 ,player.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {  //copro la prima classe di equivalenza (quando il punteggio � uguale a 0)
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("love" , scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() {  //copro la seconda classe di equivalenza (quando il punteggio � uguale a 1)
		//Arrange
		Player player = new Player("Federer", 1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("fifteen" , scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() {  //copro la terza classe di equivalenza (quando il punteggio � uguale a 2)
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("thirty" , scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() {  //copro la quarta classe di equivalenza (quando il punteggio � uguale a 3)
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("forty" , scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullNegative() {  //copro la quinta classe di equivalenza (quando il punteggio � minore di 0 )
		//Arrange
		Player player = new Player("Federer", -1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullMoreThanThree() {  //copro la sesta classe di equivalenza (quando il punteggio � maggiore di 3 )
		//Arrange
		Player player = new Player("Federer", 4);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void shouldBeTie() {  //copro la prima classe di equivalenza (quando il punteggio � in pareggio )
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void shouldNotBeTie() {  //copro la seconda classe di equivalenza (quando il punteggio non � in pareggio )
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertFalse(tie);
	}
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {  //copro la prima classe di equivalenza (quando il giocatore ha almeno 3 punti )
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {  //copro la seconda classe di equivalenza (quando il giocatore non ha almeno 3 punti )
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {  //copro la prima classe di equivalenza (quando il giocatore ha meno di 3 punti )
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome = player1.hasLessThanFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {  //copro la seconda classe di equivalenza (quando il giocatore ha 3 punti o pi�)
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveMoreThanFortyPoints() {  //copro la prima classe di equivalenza (quando il giocatore ha pi� di 3 punti )
		//Arrange
		Player player1 = new Player("Federer", 4);
		//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveMoreThanFortyPoints() {  //copro la seconda classe di equivalenza (quando il giocatore ha 3 punti o meno)
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {  //copro la prima classe di equivalenza (quando il primo giocatore ha un punto di vantaggio sul secondo)
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean outcome = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {  //copro la seconda classe di equivalenza (quando il primo giocatore non ha un punto di vantaggio sul secondo)
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantageOn() {  //copro la prima classe di equivalenza (quando il primo giocatore ha due punti di vantaggio sul secondo)
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 1);
		//Act
		boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantageOn() {  //copro la seconda classe di equivalenza (quando il primo giocatore non ha due punti di vantaggio sul secondo)
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean outcome = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertFalse(outcome);
	}
	
}
